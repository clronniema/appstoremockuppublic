package com.rp.appstoremockup.retrofit;

import com.rp.appstoremockup.app_info_data_classes.AppDetailedObject;
import com.rp.appstoremockup.app_list_data_classes.AppListObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface DataRetrofitInterface {

    @GET("rss/topfreeapplications/limit=100/json") @Headers("Content-Type: application/json")
    Call<AppListObject> downloadTop100();

    @GET("rss/topgrossingapplications/limit=10/json") @Headers("Content-Type: application/json")
    Call<AppListObject> downloadRecommended();

    @GET("lookup") @Headers("Content-Type: application/json")
    Call<AppDetailedObject> downloadAppContent(@Query("id") String appIdList);

}
