
package com.rp.appstoremockup.app_list_data_classes;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImContentType implements Serializable
{

    @SerializedName("attributes")
    @Expose
    private Attributes__ attributes;
    private final static long serialVersionUID = -4172138621274713265L;

    public Attributes__ getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes__ attributes) {
        this.attributes = attributes;
    }

}
