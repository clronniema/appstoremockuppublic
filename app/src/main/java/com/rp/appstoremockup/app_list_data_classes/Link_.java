
package com.rp.appstoremockup.app_list_data_classes;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Link_ implements Serializable
{

    @SerializedName("attributes")
    @Expose
    private Attributes________ attributes;
    private final static long serialVersionUID = -872009652036064221L;

    public Attributes________ getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes________ attributes) {
        this.attributes = attributes;
    }

}
