
package com.rp.appstoremockup.app_list_data_classes;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes implements Serializable
{

    @SerializedName("height")
    @Expose
    private String height;
    private final static long serialVersionUID = -2370763886569542272L;

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

}
