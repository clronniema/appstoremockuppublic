
package com.rp.appstoremockup.app_list_data_classes;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Example implements Serializable
{

    @SerializedName("feed")
    @Expose
    private Feed feed;
    private final static long serialVersionUID = 393519000260213757L;

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

}
