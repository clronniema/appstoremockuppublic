
package com.rp.appstoremockup.app_list_data_classes;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes_______ implements Serializable
{

    @SerializedName("label")
    @Expose
    private String label;
    private final static long serialVersionUID = -2944717464364310904L;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
