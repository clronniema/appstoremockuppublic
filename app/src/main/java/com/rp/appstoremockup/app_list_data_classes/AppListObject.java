package com.rp.appstoremockup.app_list_data_classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppListObject {
    @SerializedName("feed")
    @Expose
    private Feed object;

    public Feed getObject() {
        return object;
    }

    public void setObject(Feed object) {
        this.object = object;
    }
}
