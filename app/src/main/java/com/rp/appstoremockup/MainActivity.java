package com.rp.appstoremockup;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.rp.appstoremockup.app_info_data_classes.AppDetailedObject;
import com.rp.appstoremockup.app_info_data_classes.Result;
import com.rp.appstoremockup.app_list_data_classes.AppListObject;
import com.rp.appstoremockup.app_list_data_classes.Entry;
import com.rp.appstoremockup.retrofit.DataRetrofit;
import com.rp.appstoremockup.retrofit.DataRetrofitInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private final int TOP_100_MODULE = 0;
    private final int RECOMMENDED_MODULE = 1;

    private final static int FADE_DURATION = 500;

    private final DataRetrofitInterface dataRetrofit = DataRetrofit.getClient().create(DataRetrofitInterface.class);
    private boolean firstLoad = true;

    private EditText searchEditText;
    private List<AppDispObj> top100DispObjList = new ArrayList<>();
    private List<AppDispObj> recomDispObjList = new ArrayList<>();

    private List<AppDispObj> top100DispObjFiltList = new ArrayList<>();
    private List<AppDispObj> recomDispObjFiltList = new ArrayList<>();

    private RecomAppAdapter recomAppAdapter;
    private Top100AppOuterAdapter top100AppOuterAdapter;
    private RecyclerView recomAppRv;
    private RecyclerView top100AppRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        searchEditText = findViewById(R.id.search_edit_text);
        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

                if (recomDispObjFiltList == null) return;
                if (top100DispObjFiltList ==null) return;

                //update recommended module
                recomDispObjFiltList = getFiltDispInfoList(recomDispObjList, s.toString());
                recomAppAdapter = new RecomAppAdapter(recomDispObjFiltList);
                recomAppRv.setAdapter(recomAppAdapter);
                recomAppAdapter.notifyDataSetChanged();
                recomAppRv.scrollToPosition(0);

                TextView recomTv = (TextView) findViewById(R.id.recom_no_apps);
                if (recomDispObjFiltList.size()==0){
                    recomTv.setVisibility(View.VISIBLE);
                } else {
                    recomTv.setVisibility(View.GONE);
                }

                //update top100 module
                top100DispObjFiltList = getFiltDispInfoList(top100DispObjList, s.toString());
                top100AppOuterAdapter = new Top100AppOuterAdapter(top100DispObjFiltList);
                top100AppRv.setAdapter(top100AppOuterAdapter);
                top100AppOuterAdapter.notifyDataSetChanged();
                top100AppRv.scrollToPosition(0);

                TextView top100Tv = (TextView) findViewById(R.id.no_apps);
                if (top100DispObjFiltList.size()==0){
                    top100Tv.setVisibility(View.VISIBLE);
                } else {
                    top100Tv.setVisibility(View.GONE);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }
        });

        if (firstLoad) {
            firstLoad = false;
            new AsyncDataDownload().execute();
        } else {

        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!firstLoad) {
            // prevent app from refiring AsyncDownloadTask
            loadRecomList();
            loadTop100List();
        }
    }

    private void loadRecomList() {
        recomAppRv = findViewById(R.id.app_recom_rv);

        recomDispObjFiltList = recomDispObjList;
        recomAppAdapter = new RecomAppAdapter(recomDispObjFiltList);
        recomAppRv.setAdapter(recomAppAdapter);

        //animation to list
        recomAppRv.setTranslationX(-200);
        recomAppRv.setAlpha(0f);
        recomAppRv.animate()
                .translationX(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }

    private void loadTop100List() {
        top100AppRv = findViewById(R.id.app_top100_outer_rv);

        top100DispObjFiltList = top100DispObjList;
        top100AppOuterAdapter = new Top100AppOuterAdapter(top100DispObjFiltList);
        top100AppRv.setAdapter(top100AppOuterAdapter);

        PagerSnapHelper snapHelper = new PagerSnapHelper();
        top100AppRv.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(top100AppRv);

        //animation to list
        top100AppRv.setTranslationY(-200);
        top100AppRv.setAlpha(0f);
        top100AppRv.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }

    private void scrollRecommendedModule(int dx){
        //recomAppRv.scrollBy(-dx,0);
        //((LinearLayoutManager)recomAppRv.getLayoutManager()).scrollToPositionWithOffset(2,200);

        //TODO cannot do scroll horizontally when app listing module scroll vertically
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }

    private List<AppDispObj> getFiltDispInfoList(List<AppDispObj> dispObjFullList, String query) {

        query = query.toLowerCase();
        final List<AppDispObj> filtModelList = new ArrayList<>();
        for (AppDispObj model : dispObjFullList) {
            final String author = model.author.toLowerCase();
            final String category = model.category.toLowerCase();
            final String name = model.name.toLowerCase();
            final String summary = model.summary.toLowerCase();
            if (author.contains(query) ||
                            category.contains(query) ||
                            name.contains(query) ||
                            summary.contains(query)) {
                filtModelList.add(model);
            }
        }
        return filtModelList;
    }

    private void createMainActivitySnackBar(String msg) {
        Snackbar.make(findViewById(R.id.mainConstraintLayout), msg,
                Snackbar.LENGTH_LONG)
                .show();
    }

    private class AsyncDataDownload extends AsyncTask<String, String, String> {

        private String resp = "";
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            try {
                getAppInfo(0);
                getAppInfo(1);

                //test InvalidAppInfoType
                //getAppInfo(2);

            } catch (InvalidAppInfoType e) {
                e.printStackTrace();
                createMainActivitySnackBar(getResources().getString(R.string.invalid_app_info_type));
                resp = "fail";
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            if (result.equals("")) {
                new AsyncRecommendedAppsLoad().execute();
            }
        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "ProgressDialog",
                    "Downloading latest app information");
        }

        @Override
        protected void onProgressUpdate(String... text) {

        }

        private void synchronousCaller(final int appInfoType, Call<AppListObject> call) {
            //String result = null;
            try {
                Response<AppListObject> responseClass = call.execute();
                if (responseClass.code() == 200) {
                    AppListObject appListObject = responseClass.body();
                    if (appListObject == null) {
                        createMainActivitySnackBar(getResources().getString(R.string.invalid_data));
                    }
                    ArrayList<AppDispObj> appDispObjs = parseAppListObject(appListObject);
                    if (appInfoType == TOP_100_MODULE) {
                        top100DispObjList = getRatingAndCount(appDispObjs);
                    } else if (appInfoType == RECOMMENDED_MODULE) {
                        recomDispObjList = appDispObjs;
                    }
                } else {
                    createMainActivitySnackBar(getResources().getString(R.string.connection_refused));
                }
            } catch (IOException e) {
                createMainActivitySnackBar(getResources().getString(R.string.connection_failed));
            }
        }


        private ArrayList<AppDispObj> parseAppListObject(AppListObject appListObject) {
            ArrayList<AppDispObj> arrList = new ArrayList<>();
            List<Entry> entryList = appListObject.getObject().getEntry();
            for (int i = 0; i < entryList.size(); i++) {
                Entry entry = entryList.get(i);
                AppDispObj obj = new AppDispObj();
                obj.author = entry.getImArtist().getLabel();
                obj.category = entry.getCategory().getAttributes().getLabel();
                obj.iconLinkL = entry.getImImage().get(2).getLabel();
                obj.iconLinkM = entry.getImImage().get(1).getLabel();
                obj.iconLinkS = entry.getImImage().get(0).getLabel();
                obj.iconLinkLSize = Integer.parseInt(entry.getImImage().get(2).getAttributes().getHeight());
                obj.iconLinkMSize = Integer.parseInt(entry.getImImage().get(1).getAttributes().getHeight());
                obj.iconLinkSSize = Integer.parseInt(entry.getImImage().get(0).getAttributes().getHeight());
                obj.id = Integer.parseInt(entry.getId().getAttributes().getImId());
                obj.name = entry.getImName().getLabel();
                obj.summary = entry.getSummary().getLabel();
                obj.rating = 0.0;
                obj.userRatingCount = 0;
                obj.rank = i + 1;
                arrList.add(obj);
            }
            return arrList;
        }


        List<AppDispObj> getRatingAndCount(List<AppDispObj> listAppDispInfoObj) {
            StringBuilder idList = new StringBuilder();
            for (AppDispObj obj : listAppDispInfoObj
                    ) {
                idList.append(",").append(String.valueOf(obj.id));
            }
            idList = new StringBuilder(idList.toString().replaceFirst(",", ""));


            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            return synchronousAppInfoCaller(dataRetrofit.downloadAppContent(idList.toString()), listAppDispInfoObj);
        }

        private List<AppDispObj> synchronousAppInfoCaller(Call<AppDetailedObject> call, List<AppDispObj> listAppDispInfoObj) {
            //String result = null;
            try {
                Response<AppDetailedObject> responseClass = call.execute();
                if (responseClass.code() == 200) {
                    for (Result result : responseClass.body().getResults()) {
                        int id = result.getTrackId().intValue();
                        double avgUserRtg = result.getAverageUserRating() == null ? 0.0 : result.getAverageUserRating();
                        int userRtgCnt = result.getUserRatingCount() == null ? 0 : result.getUserRatingCount().intValue();
                        for (int i = 0; i < listAppDispInfoObj.size(); i++) {
                            if (listAppDispInfoObj.get(i).id == id) {
                                listAppDispInfoObj.get(i).userRatingCount = userRtgCnt;
                                listAppDispInfoObj.get(i).rating = avgUserRtg;
                                break;
                            }
                        }
                    }
                    return listAppDispInfoObj;
                } else {
                    createMainActivitySnackBar(getResources().getString(R.string.connection_refused));
                }
            } catch (IOException e) {
                createMainActivitySnackBar(getResources().getString(R.string.connection_failed));
            }
            return null;
        }

        private void getAppInfo(final int appInfoType) throws InvalidAppInfoType {

            try {
                Call<AppListObject> call;
                if (appInfoType == TOP_100_MODULE) {
                    call = dataRetrofit.downloadTop100();
                } else if (appInfoType == RECOMMENDED_MODULE) {
                    call = dataRetrofit.downloadRecommended();
                } else {
                    call = null;
                }
                synchronousCaller(appInfoType, call);
            } catch (NullPointerException e) {
                throw new InvalidAppInfoType(getResources().getString(R.string.invalid_app_info_type_ex), e);
            }

        }

    }

    private class AsyncRecommendedAppsLoad extends AsyncTask<String, String, String> {

        private final String resp = "";
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    loadRecomList();
                    loadTop100List();

                }
            });

            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "ProgressDialog",
                    "Loading app information");
        }

        @Override
        protected void onProgressUpdate(String... text) {

        }

    }

    class RecomAppAdapter extends RecyclerView.Adapter<RecomAppAdapter.AppDispInfoViewHolder> {
        private final List<AppDispObj> appInfoList;

        RecomAppAdapter(List<AppDispObj> appInfoList) {
            this.appInfoList = appInfoList;
        }

        public class AppDispInfoViewHolder extends RecyclerView.ViewHolder {
            final WebView appIcon;
            final TextView appName;
            final TextView appCat;
            final CardView cardView;

            AppDispInfoViewHolder(View view) {
                super(view);
                appIcon = view.findViewById(R.id.app_icon);
                appName = view.findViewById(R.id.app_name);
                appCat = view.findViewById(R.id.app_cat);
                cardView = view.findViewById(R.id.image_cardview);
            }
        }

        @Override
        public AppDispInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View childView = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_recom_child, parent, false);
            return new AppDispInfoViewHolder(childView);
        }

        @Override
        public void onBindViewHolder(AppDispInfoViewHolder holder, final int position) {
            int heightAttr = appInfoList.get(position).iconLinkLSize;
            final float scale = getResources().getDisplayMetrics().density;
            int pixels = (int) (heightAttr * scale + 0.5f);

            holder.appIcon.loadUrl(appInfoList.get(position).iconLinkL);
            ViewGroup.LayoutParams vc = holder.appIcon.getLayoutParams();
            vc.height = pixels;
            vc.width = pixels;
            holder.appIcon.setLayoutParams(vc);
            holder.appName.setText(appInfoList.get(position).name);
            holder.appCat.setText(appInfoList.get(position).category);
            setFadeAnimation(holder.itemView);
        }



        @Override
        public int getItemCount() {
            return appInfoList.size();
        }


    }

    class Top100AppOuterAdapter extends RecyclerView.Adapter<Top100AppOuterAdapter.AppDispInfoViewHolder> {

        private final List<List<AppDispObj>> appInfoListPageList;

        Top100AppOuterAdapter(List<AppDispObj> appInfoList) {
            this.appInfoListPageList = splitTop100AppAdapter(appInfoList);
        }

        public class AppDispInfoViewHolder extends RecyclerView.ViewHolder {
            final RecyclerView innerRecyclerView;

            AppDispInfoViewHolder(View view) {
                super(view);
                innerRecyclerView = view.findViewById(R.id.appTop100RecyclerView);
            }
        }

        @Override
        public AppDispInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //inflate the layout file
            View childView = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_inner_recycler_view, parent, false);
            return new AppDispInfoViewHolder(childView);
        }

        @Override
        public void onBindViewHolder(AppDispInfoViewHolder holder, final int position) {
            RecyclerView innerRecyclerView = holder.innerRecyclerView;
            Top100AppAdapter top100AppPageAdapter = new Top100AppAdapter(appInfoListPageList.get(position));
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(innerRecyclerView.getContext(), LinearLayoutManager.VERTICAL);
            innerRecyclerView.addItemDecoration(dividerItemDecoration);
            innerRecyclerView.setAdapter(top100AppPageAdapter);
            innerRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            scrollRecommendedModule(recyclerView.computeVerticalScrollOffset());
                        }
                    });
        }

        @Override
        public int getItemCount() {
            return appInfoListPageList.size();
        }

        private List<List<AppDispObj>> splitTop100AppAdapter(List<AppDispObj> appInfoList){

            List<List<AppDispObj>> appInfoListPageList = new ArrayList<>();
            if (appInfoList==null) return appInfoListPageList;

            int pageCount;
            pageCount = appInfoList.size() / 10;
            if (pageCount > 0){
                for (int i = 0; i < pageCount; i++){
                    appInfoListPageList.add(appInfoList.subList(i * 10, (i+1)*10));
                }
            }
            if (appInfoList.size() % 10 != 0){
                pageCount++;
                appInfoListPageList.add(appInfoList.subList((pageCount-1) * 10, appInfoList.size()));
            }
            return appInfoListPageList;
        }

    }

    class Top100AppAdapter extends RecyclerView.Adapter<Top100AppAdapter.AppDisplayInfoViewHolder> {

        private final List<AppDispObj> appInfoList;

        Top100AppAdapter(List<AppDispObj> appInfoList) {
            this.appInfoList = appInfoList;
        }

        public class AppDisplayInfoViewHolder extends RecyclerView.ViewHolder {
            final WebView appIcon;
            final TextView appName;
            final TextView appCat;
            final TextView appRateCount;
            final TextView appRankingNum;
            final RatingBar appRating;
            final CardView cardView;

            AppDisplayInfoViewHolder(View view) {
                super(view);
                appIcon = view.findViewById(R.id.app_icon);
                appName = view.findViewById(R.id.app_name);
                appCat = view.findViewById(R.id.app_cat);
                cardView = view.findViewById(R.id.image_cardview);
                appRateCount = view.findViewById(R.id.app_rate_count);
                appRankingNum = view.findViewById(R.id.app_ranking_num);
                appRating = view.findViewById(R.id.app_rating);
            }
        }

        @Override
        public AppDisplayInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //inflate the layout file
            View childView = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_top100_child, parent, false);
            return new AppDisplayInfoViewHolder(childView);
        }

        @Override
        public void onBindViewHolder(AppDisplayInfoViewHolder holder, final int position) {
            int heightAttr = appInfoList.get(position).iconLinkLSize;
            final float scale = getResources().getDisplayMetrics().density;
            int pixels = (int) (heightAttr * scale + 0.5f);

            holder.appIcon.loadUrl(appInfoList.get(position).iconLinkL);
            ViewGroup.LayoutParams vc = holder.appIcon.getLayoutParams();
            vc.height = pixels;
            vc.width = pixels;
            holder.appIcon.setLayoutParams(vc);

            if (position % 2 == 1){
                holder.cardView.setRadius(pixels / 2);
            }
            holder.appName.setText(appInfoList.get(position).name);
            holder.appCat.setText(appInfoList.get(position).category);

            holder.appRankingNum.setText(String.valueOf(appInfoList.get(position).rank));
            String appRateCountText = "(" + String.valueOf(appInfoList.get(position).userRatingCount) + ")";
            holder.appRateCount.setText(appRateCountText);
            Double rating = appInfoList.get(position).rating;
            holder.appRating.setRating(rating == null ? 0.0f : Float.valueOf(rating.toString()));

            setFadeAnimation(holder.itemView);
        }

        @Override
        public int getItemCount() {
            return appInfoList.size();
        }

    }

    class AppDispObj {

        String iconLinkS, iconLinkM, iconLinkL;
        String name, category, author, summary;
        Integer id, userRatingCount;
        Integer iconLinkSSize, iconLinkMSize, iconLinkLSize;
        Double rating;
        Integer rank;

    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (recomDispObjList.size()==0 || top100DispObjList.size()==0) {
                new AsyncDataDownload().execute();
            }
        }
    }

    class InvalidAppInfoType extends Exception {
        InvalidAppInfoType(String message, Throwable throwable) {
            super(message, throwable);
        }
    }

}